# Instalacao do Node e Mysql

Requisitos para instalação do NodeJs e Mysql 5.7.

## Configurações Iniciais

Instruções para o processo de instalação. 

### Pré-requisitos

Realizar a instalação na ordem.

* [Docke OS X](https://docs.docker.com/mac/started/)

### Instalando Containers

#### Mysql

```shell
docker run -dit --name mysql --hostname mysql  -e MYSQL_ROOT_PASSWORD=senha123 -e TZ=America/Campo_Grande -p 3306:3306 mysql:5.7
```

#### NodeJS

```shell
docker run -dit --name graphql-node --hostname graphql-node -e TZ=America/Campo_Grande --link mysql:mysql -p 3000:3000 -v $PWD:/opt/graphql-node-api node:8.6.0
```